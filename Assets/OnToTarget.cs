using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class OnToTarget : MonoBehaviour
{
    
    public GameObject target1;
    public NavMeshAgent agent;
    public Rigidbody rb;
    public Animator anim;
    public bool isMoving;
    //public bool isRunning;
    public float WaitWalk = 0.8f;
   
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        anim.SetBool("IsMoving", true);

    }

    private void Update()
    {
        StartCoroutine(StartScene());
        anim.SetBool("IsMoving", true);
       
        if(isMoving == true)
        {
            StartCoroutine(DrunkWalk());
        }
        /*
        if (isMoving == true)
        {
            
        }

        if (isRunning == true)
        {
            anim.SetBool("IsRunning", true);
        }
        */
        /*
        if(Vector3.Distance(transform.position, target1.transform.position) > 12f)
        {
            anim.SetBool("IsRunning", true);
            anim.SetBool("IsMoving", false);
            agent.speed = 10f;

        } else if (Vector3.Distance(transform.position, target1.transform.position) > 2f)
        {
            anim.SetBool("IsRunning", false);
            anim.SetBool("IsMoving", true);
            agent.speed = 3.5f;

        }
        */
    }

    IEnumerator DrunkWalk()
    {
        agent.speed = 0.3f;
        yield return new WaitForSeconds(WaitWalk);
        agent.speed = 0.9f;
    }

    IEnumerator StartScene()
    {
        yield return new WaitForSeconds(2f);
        agent.SetDestination(target1.transform.position);
    }

}
