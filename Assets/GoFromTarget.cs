using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GoFromTarget : MonoBehaviour
{
    public GameObject target;
    public NavMeshAgent agent;
    public float WaitTime = 1f;

    private void Start()
    {
        StartCoroutine(Wait());
    }
    void Update()
    {
        
    }

    public IEnumerator Wait()
    {
        yield return new WaitForSeconds(WaitTime);
        XDDD();
        yield return new WaitForSeconds(WaitTime);
        Move();
    }
    public void XDDD()
    {  
        agent.SetDestination(target.transform.position);
        
    }

    private void Move()
    {
        agent.SetDestination(new Vector3(0,0,0));
        //StopAllCoroutines();
    }
   

}
